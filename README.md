# DevOps Course Assignment Salt Scripts for Nginx and Tomcat Setup

This repository contains Salt scripts developed as part of a DevOps course assignment. The scripts aim to set up Nginx as a load balancer routing requests to two Tomcat nodes acting as web servers.

## Salt Scripts

### init_nginx.sls

Installs Nginx on the minion node 'node1.local'. The load balancer is configured to route requests to Tomcat nodes based on predefined weights.

### init_tomcat1.sls

Installs Tomcat version 9.0.36 on 'node2.local' under /usr/local. Configures Tomcat as a web server with a valid HTML homepage containing the string 'ppttwzbanfmq'.

### init_tomcat2.sls

Installs Tomcat version 9.0.36 on 'node3.local' under /usr/local. Configures Tomcat as a web server with a valid HTML homepage containing the string 'nfrwzpcadbfp'.


## Usage

To apply the Salt states, use the following procedure:

```
cd feature166
salt node2.local state.apply tomcat1
salt node3.local state.apply tomcat2
salt node1.local state.apply nginx
```

## Important Notes

- The Salt version used for this exercise is 3005.

- Config and data files needed are in the feature branch folder 'feature166/'. Use 'salt://' as the path to access them from within Salt scripts.

- Do not use the 'service.running' module of Salt as it is not supported in the current execution setup.


## Additional Resources

- [Salt Documentation](https://docs.saltproject.io/en/latest/contents.html)
- [Nginx Load Balancer Mode](https://nginx.org/en/)
- [Tomcat Documentation](https://tomcat.apache.org/)


## Timetable and Submission

Ensure completion before the due date. If successful, submit a pull request via the Gitea Web application.
